(function ($, Drupal) {
  "use strict";

  Drupal.behaviors.agitraining = {
    findEventFilterOption: function () {
      return $(
        "input[name=field_event_frequency_target_id]:checked",
        "#views-exposed-form-events-events"
      );
    },
    attach: function (context, settings) {
      var agi = Drupal.behaviors.agitraining;

      // Modals behabiour.
      $(".modal").on("hidden.bs.modal", function () {
        $(".modal").find("video").trigger("pause");
      });

      // Events exposed filter.
      var eventFilterOption = agi.findEventFilterOption();
      eventFilterOption.parent().find("label").addClass("selected");

      $("#views-exposed-form-events-events input").on("change", function () {
        $(this).parent().find("label").removeClass("selected");

        eventFilterOption = agi.findEventFilterOption();
        eventFilterOption.parent().find("label").addClass("selected");
      });

      // Search behavior.
      $("#block-exposedformsearchsearch-page")
        .find("img")
        .on("click", function () {
          $(this)
            .closest("#block-exposedformsearchsearch-page")
            .find("form")
            .submit();
        });
    },
  };
})(jQuery, Drupal);
