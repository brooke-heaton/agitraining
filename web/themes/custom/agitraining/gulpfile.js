'use strict';
const sass = require('gulp-sass')(require('sass'));
var gulp = require('gulp');
var concat = require('gulp-concat');
sass.compiler = require('node-sass');
gulp.task('sass', function () {
  return gulp.src('./scss/**/*.scss')
    .pipe(concat('style.scss'))
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./css/'));
});
