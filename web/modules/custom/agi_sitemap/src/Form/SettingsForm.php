<?php

namespace Drupal\agi_sitemap\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure AGI Sitemap settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'agi_sitemap_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['agi_sitemap.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['description'] = [
      '#type' => 'markup',
      '#title' => $this->t('Run Cron every X hours and X seconds'),
    ];
    $form['hours'] = [
      '#type' => 'number',
      '#title' => $this->t('Hours'),
      '#default_value' => $this->config('agi_sitemap.settings')->get('hours'),
    ];
    $form['seconds'] = [
      '#type' => 'number',
      '#title' => $this->t('seconds'),
      '#default_value' => $this->config('agi_sitemap.settings')->get('seconds'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!is_numeric($form_state->getValue('hours'))) {
      $form_state->setErrorByName('hours', $this->t('The value is not correct.'));
    }
    if (!is_numeric($form_state->getValue('seconds'))) {
      $form_state->setErrorByName('seconds', $this->t('The value is not correct.'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('agi_sitemap.settings')
      ->set('hours', $form_state->getValue('hours'))
      ->save();
    $this->config('agi_sitemap.settings')
      ->set('seconds', $form_state->getValue('seconds'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
