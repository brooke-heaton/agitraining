<?php

/**
 * @file
 * Primary module hooks for AGI Sitemap module.
 *
 * @DCG
 * This file is no longer required in Drupal 8.
 * @see https://www.drupal.org/node/2217931
 */


/**
 * Implements hook_cron().
 */
function agi_sitemap_cron() {

  // Set the default interval to regenerate course_date node sitemap values.
  $interval = \Drupal::config('agi_sitemap.settings')->get('hours') * 60 * 60 + \Drupal::config('agi_sitemap.settings')->get('seconds');
  $now = time();
  $last_run = \Drupal::state()->get('agi_sitemap_last_run');

  // See if more than the interval time has passed since last update.
  if ($now + $interval >= $last_run) {

    // Define the node type we want to access.
    $node_type = 'course_date';
    // Perform db_query() to get nid's of results.
    $result = \Drupal::database()->query('SELECT n.nid
    FROM {node} n
    LEFT JOIN node__field_cd_event e ON n.nid = e.entity_id
    LEFT JOIN  commerce_product_variation__67b1a73399 v ON e.field_cd_event_target_id = v.entity_id
    WHERE n.type = :node_type
    AND v.field_event_start_date_value > NOW()', [':node_type' => $node_type]);

    $upcoming_course_date_nids = $result->fetchCol();

    // Set all course_date nodes to status = 0 so they will not be added to sitemap.
    \Drupal::database()->update('xmlsitemap')
      ->fields([
        'status' => '0',
        'status_override' => 0,
      ])
      ->condition('subtype', 'course_date', '=')
      ->execute();


    // Update the {xmlsitemap}.status value to reflect upcoming or past events.
    foreach ($upcoming_course_date_nids as $key => $value) {
      \Drupal::database()->update('xmlsitemap')
        ->fields([
          'status' => 1,
          'status_override' => 1,
        ])
        ->condition('id', $value)
        ->execute();
    }

    // Set the flag to regenerate sitemap.
    \Drupal::state()->set('xmlsitemap_regenerate_needed', TRUE);

    // Set the next time to execute.
    \Drupal::state()->set('agi_sitemap_last_run', $now);
  }
}

function agi_sitemap_xmlsitemap_link_alter(&$link) {
  if ($link['subtype'] == 'subject' || $link['subtype'] == 'category_location_page' || $link['subtype'] == 'seconday_location_category_page') {
    $link['changefreq'] = XMLSITEMAP_FREQUENCY_DAILY;
    $link['lastmod'] = time();
  }
}

