<?php

namespace Drupal\agi_migrate\Feeds\Target;

use Drupal\feeds\Feeds\Target\EntityReference;

/**
 * Defines a commerce_price field mapper.
 *
 * @FeedsTarget(
 *   id = "order_types",
 *   field_types = {"order_types"}
 * )
 */
class CommerceOrderTypes extends EntityReference {

}
