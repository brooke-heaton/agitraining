<?php

namespace Drupal\agi_migrate\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;

/**
 * Migrate form class.
 */
class MigrateForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'agi_migrate_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Form constructor.
    $form = parent::buildForm($form, $form_state);

    $form['migrate_category_fields'] = [
      '#type' => 'managed_file',
      '#title' => t('Category CSV File'),
      '#description' => t('The CSV will map the fields to the category Content Type.'),
      '#upload_location' => 'private://category',
      '#upload_validators' => [
        'file_validate_extensions' => ['csv'],
      ],
		];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_file = $form_state->getValue('migrate_category_fields', 0);

    if (isset($form_file[0]) && !empty($form_file[0])) {
      $file_entity = File::load($form_file[0]);
      $file = fopen($file_entity->getFileUri(), "r");
      $nodes_info = [];

      while (!feof($file)) {
        $fields = fgetcsv($file);

        $nodes_info[] = [
          'title' => $fields[0],
          'field_category_roles' => $fields[1],
          'field_category_understand' => $fields[2],
          'field_category_optional_topics' => $fields[3],
          'field_category_instructor_xp' => $fields[4],
          'field_category_instructor_skills' => $fields[5],
          'field_category_skills_learned' => $fields[6],
        ];
      }

      fclose($file);

      $operations = [
        ['update_category_nodes', [$nodes_info]],
      ];

      $batch = [
        'title' => $this->t('Updating All Category Nodes ...'),
        'operations' => $operations,
        'finished' => 'update_category_nodes_finished',
      ];

      batch_set($batch);
    }

    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    // This function returns the name of the settings files we will
    // create / use.
    return [
      'agi_migrate.settings',
    ];
  }

}
