<?php

namespace Drupal\agi_reviews\Controller;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\Core\Controller\ControllerBase;

class AgiReviews extends ControllerBase {
  public function reviewsPage($nid, $course) {
    // Checking if the category is valid.
    $query = \Drupal::entityQuery('node')
      ->condition('type', 'subject')
      ->condition('nid', $nid, '=');

    $results = $query->execute();
    $category_node = $results;

    if ($category_node) {
      return [
        '#theme' => 'agi_reviews_page',
      ];
    }
    else {
      throw new NotFoundHttpException();
    }
  }
}
