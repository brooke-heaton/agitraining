<?php

namespace Drupal\agi_blocks\Plugin\Condition;

use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\taxonomy\Entity\Term;

/**
 * Provides a 'Parent Category Match' condition.
 *
 * @Condition(
 *   id = "parent_category_match",
 *   label = @Translation("Parent Category Match"),
 *   context_definitions = {
 *     "node" = @ContextDefinition("entity:node", label = @Translation("Node"))
 *   }
 * )
 */
class ParentCategoryMatch extends ConditionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $entityStorage;

  /**
   * Creates a new NodeType instance.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $entity_storage
   *   The entity storage.
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(EntityStorageInterface $entity_storage, array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityStorage = $entity_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('entity_type.manager')->getStorage('taxonomy_vocabulary'),
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  public function getVocabTerms($vid) {
    $query = \Drupal::entityQuery('taxonomy_term');
    $query->condition('vid', $vid);
    $tids = $query->execute();
    return Term::loadMultiple($tids);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $options = [];
    $parent_categories = $this->getVocabTerms('parent_category');
    foreach ($parent_categories as $term) {
      $options[$term->id()] = $term->label();
    }
    $form['terms'] = [
      '#title' => $this->t('Parent Category to Match'),
      '#type' => 'checkboxes',
      '#options' => $options,
      '#default_value' => $this->configuration['terms'],
    ];
    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['terms'] = array_filter($form_state->getValue('terms'));
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {
    if (count($this->configuration['terms']) > 1) {
      $term_ids = $this->configuration['terms'];
      foreach ($term_ids as $tid) {
        $terms[] = Term::load($tid)->get('name')->value;
      }
      $last = array_pop($terms);
      $terms = implode(', ', $terms);
      return $this->t('The Parent Category is @terms or @last', [
        '@terms' => $terms,
        '@last' => $last,
      ]);
    }
    $tid = reset($this->configuration['terms']);
    $term = Term::load($tid)->get('name')->value;
    return $this->t('The Parent Category is @term', ['@term' => $term]);
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    if (empty($this->configuration['terms']) && !$this->isNegated()) {
      return TRUE;
    }
    $node = $this->getContextValue('node');
    $node_bundle = $node->bundle();
    switch ($node_bundle) {
      case 'category_location_page':
      case 'seconday_location_category_page':
        $category = $node->get('field_cat_for_location')->first()->entity;
        break;
      case 'subject':
        $category = $node;
      default:
        $category = NULL;
    }
    if (!empty($category) && !$category->get('field_parent_category')->isEmpty()) {
      $category_parent_category = $category->get('field_parent_category')
          ->first()
          ->getValue()['target_id'] ?? NULL;
      return !empty($this->configuration['terms'][$category_parent_category]);
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['terms' => []] + parent::defaultConfiguration();
  }

}
