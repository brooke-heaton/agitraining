<?php

namespace Drupal\agi_blocks\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\agi_blocks\AgiService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block with the reviews for a specific course.
 *
 * @Block(
 *   id = "agi_block_reviews_block",
 *   admin_label = @Translation("AGI Reviews Block"),
 * )
 */
class ReviewsBlock extends BlockBase implements ContainerFactoryPluginInterface {
  /**
   * @var AccountInterface $account
   */
  protected $account;

  /**
   * Variable that will store the service.
   *
   * @var \Drupal\agi_blocks\AgiService
   */
  protected $agiService;

  /**
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\Core\Session\AccountInterface $account
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AccountInterface $account, AgiService $agiService) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->account = $account;
    $this->agiService = $agiService;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get('agi.service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Loading the service into a variable.
    $agi_service = $this->agiService;

    // Getting the current node.
    $current_node = \Drupal::routeMatch()->getParameter('node');

    // Loading the config.
    $config = $this->getConfiguration();

    // Getting the filered config.
    $block_header = $agi_service->filterConfigText($config['block_header'], $current_node);
    $block_rating_text = $agi_service->filterConfigText($config['block_rating_text'], $current_node);
    $block_rating_summary_text = $agi_service->filterConfigText($config['block_rating_summary_text'], $current_node);

    // Getting the info from the reviews.
    $reviews_info = $agi_service->getReviewsInfo($current_node);

    if (!$reviews_info) {
      return ['#markup' => ''];
    }

    return [
      '#theme' => 'agi_reviews_blocks',
      '#block_header' => $block_header ?? '',
      '#average_review' => $reviews_info['average'] ?? 0,
      '#total_reviewers' => $reviews_info['reviewers'],
      '#block_rating_text' => $block_rating_text ?? '',
      '#block_rating_summary_text' => str_replace(['%stars', '%reviewers'], [$reviews_info['average'], $reviews_info['reviewers']], $block_rating_summary_text) ?? '',
      '#latest_review' => html_entity_decode($reviews_info['latest_review'], ENT_QUOTES) ?? '',
      '#latest_review_date' => date('Y-m-d', $reviews_info['latest_review_date']),
      '#latest_reviewer' => $reviews_info['latest_reviewer'] ?? '',
      '#category_id' => $reviews_info['category_id'],
      '#category_label' => $reviews_info['category_label'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['block_header'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Block header'),
      '#default_value' => $config['block_header'] ?? '',
    ];

    $form['block_rating_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Rating text'),
      '#default_value' => $config['block_rating_text'] ?? '',
    ];

    $form['block_rating_summary_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Rating summary text'),
      '#default_value' => $config['block_rating_summary_text'] ?? '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    //parent::blockSubmit($form, $form_state);

    $values = $form_state->getValues();

    $this->configuration['block_header'] = $values['block_header'];
    $this->configuration['block_rating_text'] = $values['block_rating_text'];
    $this->configuration['block_rating_summary_text'] = $values['block_rating_summary_text'];
  }
}
