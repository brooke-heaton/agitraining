<?php

namespace Drupal\agi_blocks\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\Entity\Node;
use Drupal\agi_blocks\AgiService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block showing a video if the course is online.
 *
 * @Block(
 *   id = "agi_block_category_additional_block",
 *   admin_label = @Translation("AGI Category Additional Courses"),
 * )
 */
class CategoryAdditionalCourses extends BlockBase implements ContainerFactoryPluginInterface {
  /**
   * @var AccountInterface $account
   */
  protected $account;

  /**
   * Variable that will store the service.
   *
   * @var \Drupal\agi_blocks\AgiService
   */
  protected $agiService;

  /**
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\Core\Session\AccountInterface $account
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AccountInterface $account, AgiService $agiService) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->account = $account;
    $this->agiService = $agiService;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get('agi.service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Loading the service into a variable.
    $agi_service = $this->agiService;

    // Getting the current node.
    $current_node = \Drupal::routeMatch()->getParameter('node');

    // Markup text for the LB.
    $markup_text = t('Placeholder for the "AGI Category Additional Courses Block."');

    if ($current_node) {
      // Loading the config.
      $config = $this->getConfiguration();

      // Getting the filered config.
      $block_header = $agi_service->filterConfigText($config['block_header'], $current_node);

      // Returning the Related Certificate Programs.
      $certificate_programs = $current_node->get('field_certificate_programs')->getValue();

      foreach ($certificate_programs as $certificate_program) {
        $certificate_program_nids[] = $certificate_program['target_id'];
      }

      if (!empty($certificate_program_nids)) {
        $certificate_programs_nodes = Node::loadMultiple($certificate_program_nids);

        foreach ($certificate_programs_nodes as $certificate_programs_node) {
          $programs[] = [
            'id' => $certificate_programs_node->id(),
            'alias' => \Drupal::service('path_alias.manager')->getAliasByPath('/node/' . $certificate_programs_node->id()),
            'label' => $certificate_programs_node->label(),
          ];
        }
      }

      // Checking if the location is "Online".
      if (!empty($programs)) {
        return [
          '#theme' => 'agi_category_additional_block',
          '#block_header' => $block_header ?? '',
          '#programs' => $programs,
        ];
      }

      $markup_text = '';
    }

    // Default for the Layout Builder.
    return [
      '#markup' => $markup_text,
    ];

  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['block_header'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Block header'),
      '#default_value' => $config['block_header'] ?? '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $this->configuration['block_header'] = $values['block_header'];
  }
}
