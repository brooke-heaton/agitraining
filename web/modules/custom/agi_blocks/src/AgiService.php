<?php

namespace Drupal\agi_blocks;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\node\Entity\Node;
use Drupal\media\Entity\Media;
use Drupal\file\Entity\File;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class that define the service that handles the AGI Blocks.
 */
class AgiService implements ContainerFactoryPluginInterface {

  /**
   * Stores the configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * When the service is created, set the initial value variables.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * Custom method to filter the token value.
   */
  public function filterConfigText($config, $current_node) {
    // Load the token service.
    $token = \Drupal::token();

    return isset($config) ? $token->replace($config, ['node' => $current_node]) : '';
  }

  /**
   * Get the average reviews for the NID.
   */
  public function getReviewsInfo($current_node) {
    if ($current_node instanceof NodeInterface) {
      // If there's no node, the title is empty.
      $category_label = '';

      // Loading the referenced node.
      switch ($current_node->bundle()) {
        case 'category_location_page':
        case 'seconday_location_category_page':
          $category = $current_node->field_cat_for_location->target_id;
          $category_label = $current_node->field_cat_for_location->entity->label();
          break;
        case 'subject':
          $category = $current_node->id();
          $category_label = $current_node->label();
          break;
        case 'course':
          $primary_category = !$current_node->field_primary_category->isEmpty() ? $current_node->field_primary_category->first() : null;
          $category = !empty($primary_category->target_id) ? $primary_category->target_id : null;
          if($primary_category && !empty($primary_category->entity->label()) ) {
            $category_label = $primary_category->entity->label();
          }
      }

      $reviews_nids = \Drupal::entityQuery('node')
        ->condition('type', 'reviews')
        ->condition('status', 1)
        ->condition('body', '', '<>')
        ->condition('field_review_category', $category)
        ->sort('created', 'DESC')
        ->execute();

      $reviews = Node::loadMultiple($reviews_nids);

      if (!empty($reviews)) {
        $sum_rating = 0;

        foreach($reviews as $review) {
          $sum_rating += $review->field_review_rating->value;
        }

        $latest_review = reset($reviews);

        return [
          'average' => (int) round(number_format($sum_rating / count($reviews_nids), 1)),
          'reviewers' => count($reviews_nids),
          'latest_review' => $latest_review->body->value,
          'latest_reviewer' => $latest_review->field_reviewer_name->value,
          'latest_review_date' => $latest_review->getCreatedTime(),
          'category_id' => $category,
          'category_label' => $category_label,
        ];
      }
      else {
        return null;
      }
    }
  }

  /**
   * Get the average reviews for all reviews.
   */
  public function getAllReviewsInfo($current_node) {
    $reviews_nids = \Drupal::entityQuery('node')
      ->condition('type', 'reviews')
      ->condition('status', 1)
      ->condition('body', '', '<>')
      ->sort('created', 'DESC')
      ->execute();

    $reviews = Node::loadMultiple($reviews_nids);

    if (!empty($reviews)) {
      $sum_rating = 0;

      foreach($reviews as $review) {
        $sum_rating += $review->field_review_rating->value;
      }

      $latest_review = reset($reviews);

      return [
        'average' => (int) round(number_format($sum_rating / count($reviews_nids), 1)),
        'number_of_reviews' => count($reviews_nids),
      ];
    }
    else {
      return null;
    }
  }

  /**
   * Get the Category Header Subtext for the NID.
   */
  public function getCategoryHeaderSubtext($current_node) {
    // Load the reference field.
    if ($current_node instanceof NodeInterface) {
      $category = Node::load($current_node->field_cat_for_location->target_id);
      $header_subtext = $category->field_black_header_subtext->value;
    }

    return $header_subtext ?? '';
  }

  /**
   * Get the Location Address given a node.
   */
  public function getLocationAddress($current_node) {
    // Load the reference field.
    if ($current_node instanceof NodeInterface) {
      switch ($current_node->bundle()) {
        case 'seconday_location_page':
          $node = $current_node;
          break;
        case 'seconday_location_category_page':
          $node = Node::load($current_node->field_secondary_location->target_id);
          break;
        default:
          $node = $current_node;
      }

      $address = [
        'street' => isset($node->field_address_street) ? $node->field_address_street->value : '',
        'address_2' => isset($node->field_address_line_2) ? $node->field_address_line_2->value : '',
        'city' => isset($node->field_address_city) ? $node->field_address_city->value : '',
        'state' => isset($node->field_state_abbreviated_) ? $node->field_state_abbreviated_->value : '',
        'zip' => isset($node->field_address_zip) ? $node->field_address_zip->value : '',
        'location' => $node->label(),
      ];
    }

    return $address ?? t('Placeholder for the "Location Address" custom block');
  }

  /**
   * Get the Secondary Location Images node.
   */
  public function getSecondaryImages($current_node) {
    $fids = [];

    // Load the reference field.
    if ($current_node instanceof NodeInterface) {
      $secondary_location = Node::load($current_node->field_secondary_location->target_id);
      $images_media = $secondary_location->get('field_location_image_media')->getValue();


      foreach ($images_media as $image_media) {
        $media = Media::load($image_media['target_id']);

        if ($media->field_media_image->entity) {
          $fids[] = $media->field_media_image->entity->getFileUri();
        }
      }
    }

    return $fids ?? t('Placeholder for the "AGI Request Info Image Block" custom block');
  }

  /**
   * Get the Request Info Image URI.
   */
  public function getRequestInfoImageUri($current_node) {
    // Load the reference field.
    if ($current_node instanceof NodeInterface) {
      switch ($current_node->bundle()) {
        case 'subject':
          $category_node = $current_node;
          break;
        case 'seconday_location_category_page':
          $category_node = Node::load($current_node->field_cat_for_location->target_id);
          break;
        default:
          $category_node = '';
          break;
      }

      if (!empty($category_node)) {
        $request_info_media_id = $category_node->get('field_left_header_image_media')->getValue();

        // Checking if there's an image.
        if ($request_info_media_id) {
          $request_info_media = Media::load($request_info_media_id[0]['target_id']);
          $request_info_uri = $request_info_media->field_media_image->entity->getFileUri();
        }
      }
      else {
        $request_info_uri = 'public://training-courses.jpg';
      }
    }

    return $request_info_uri ?? '';
  }

  /**
   * Get the available classes.
   */
  public function getAvailableClasses() {
    $category_nids = \Drupal::entityQuery('node')
      ->condition('type','subject')
      ->execute();

    $category_nodes = Node::loadMultiple($category_nids);
    $categories = [];

    foreach ($category_nodes as $category_node) {
      $categories[] = [
        'alias' => \Drupal::service('path_alias.manager')->getAliasByPath('/node/' . $category_node->id()),
        'label' => $category_node->label(),
      ];
    }

    return $categories;
  }

  /**
   * Get the external links for the books.
   */
  public function getBooksLinksFields($current_node) {
    return 'Test';
  }
}
