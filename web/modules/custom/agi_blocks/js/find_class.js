(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.find_class = {
    attach: function (context, settings) {
      $("#find-class").change(function () {
        var trainingClassUrl = this.value;

        window.location.href = trainingClassUrl;
      });
    }
  };
})(jQuery, Drupal);
