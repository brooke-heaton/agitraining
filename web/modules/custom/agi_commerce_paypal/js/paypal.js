(function ($, Drupal) {
  Drupal.behaviors.paypal = {
    getPaypalRadioValue: function () {
      return $(
        "input[id^='edit-payment-information-payment-method-new-paypal-checkout-paypal-wps']:checked"
      ).val();
    },
    togglePaypalButton: function (mode) {
      $("div.agi-paypal-button").css({display: mode});
    },
    toggleSubmitButton: function (mode) {
      $("input.form-submit").css({display: mode});
    },
    attach: function (context, settings) {
      var self = this;
      var paypalValue = self.getPaypalRadioValue();
      paypalValue
        ? self.togglePaypalButton("block")
        : self.togglePaypalButton("none");
      paypalValue
        ? self.toggleSubmitButton("none")
        : self.toggleSubmitButton("block");
    },
  };
})(jQuery, Drupal);
