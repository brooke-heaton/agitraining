<?php

namespace Drupal\agi_near_me\Controller;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\Core\Controller\ControllerBase;

class AgiNearMe extends ControllerBase {
  public function allPage($category) {
    // Checking if the category is valid.
    $query = \Drupal::entityQuery('node')
      ->condition('type', 'subject')
      ->condition('field_short_category_name', str_replace('-', ' ', $category), '=');

    $results = $query->execute();
    $category_node = end($results);

    if ($category_node) {
      return [
        '#theme' => 'agi_near_me_page',
      ];
    }
    else {
      throw new NotFoundHttpException();
    }
  }
}
