<?php

/**
 * @file
 * Injects Schema.org structured data into components.
 */

 use Drupal\media\Entity\Media;
use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\agi_schema\AgiSchemaService;

/**
 * Implements hook_preprocess_views_view().
 */
function agi_schema_preprocess_views_view(&$variables) {
  $view = $variables['view'];

  if ($view->id() === 'events') {
    $results = $variables['view']->result;
    $schema = [];
    $base_path = \Drupal::request()->getSchemeAndHttpHost();
    $current_node = \Drupal::routeMatch()->getParameter('node');
    $current_node_bundle = $current_node ? $current_node->bundle() : '';

    foreach ($results as $result) {
      $course_id = $result->_entity->field_event_name->target_id;
      $course_price = $result->_entity->commerce_price->first()->getValue();
      $event_name = $result->_relationship_entities['field_event_name']->label();
      $event_date = $result->_entity->field_event_start_date->first()->getValue();
      $event_start_date = date('Y-m-d', strtotime($event_date['value']));
      $event_end_date = date('Y-m-d', strtotime($event_date['end_value']));
      $event_location = $result->_relationship_entities["field_event_location"]->field_city_for_reference_pages->value;
      $event_url = $base_path . \Drupal::service('path_alias.manager')->getAliasByPath('/node/' . $course_id);
      $site_logo = \Drupal::theme()->getActiveTheme()->getLogo();

      // Treating online events.
      if ($event_location === 'Online') {
        $event_attendance_mode = 'http://schema.org/OnlineEventAttendanceMode';

        $event_location_data = [
          '@type' => 'VirtualLocation',
          'url' => $event_url,
        ];

        // Secondary location page, despite being online, needs to show
        // the location.
        if ($current_node_bundle === 'seconday_location_category_page') {
          $event_attendance_mode = 'http://schema.org/MixedEventAttendanceMode';

          $event_location_data = [
            '@type' => 'Place',
            'name' => 'American Graphics Institute',
            'address' => [
              '@type' => 'PostalAddress',
              'addressLocality' => $current_node->field_secondary_location->entity->field_address_city->value,
              'addressRegion' => $current_node->field_secondary_location->entity->field_state_abbreviated_->value,
            ],
          ];
        }
      } else {
        $event_attendance_mode = 'http://schema.org/OfflineEventAttendanceMode';

        $event_location_data = [
          '@type' => 'Place',
          'name' => 'American Graphics Institute',
          'address' => [
            '@type' => 'PostalAddress',
            'name' => $event_location,
          ],
        ];
      }

      // Fetching the results to add to the schema.
      $schema[] = [
        '@context' => 'https://schema.org',
        '@type' => 'EducationEvent',
        'name' => $event_name,
        'url' => $event_url,
        'image' => $base_path . '/' . $site_logo,
        'eventStatus' => 'https://jsonld.com/event/concert-multiple-performers/',
        'eventAttendanceMode' => $event_attendance_mode,
        'startDate' => $event_start_date,
        'endDate' => $event_start_date,
        'description' => $event_name . ', ' . $event_location . ' on ' . $event_start_date,
        'eventStatus' => 'http://schema.org/EventScheduled',
        'performer' => [
          '@type' => 'Organization',
          'name' => 'American Graphics Institute',
        ],
        'organizer' => [
          '@type' => 'Organization',
          'name' => 'American Graphics Institute',
          'url' => 'https://www.agitraining.com/',
        ],
        'location' => $event_location_data,
        'offers' => [
          '@type' => 'Offer',
          'url' => $event_url,
          'validFrom' => '1969-12-31T19:00:00-05:00',
          'priceCurrency' => 'USD',
          'availability' => 'http://schema.org/InStock',
          'price' => number_format($course_price['number'], 2, '.', ''),
        ],
      ];
    }

    $view->element['#attached']['html_head'][] = [
      [
        '#tag' => 'script',
        '#attributes' => [
          'type' => 'application/ld+json',
        ],
        '#value' => json_encode($schema),
      ],
      'agi_events_schema',
    ];
  }

  if ($view->id() === 'category_location_view') {

  }
}

/**
 * Implements hook_preprocess_block().
 */
function agi_schema_preprocess_block(&$variables) {
  if ($variables['elements']['#plugin_id'] === 'agi_block_reviews_block') {
    // Getting the current node.
    $current_node = \Drupal::routeMatch()->getParameter('node');

    // Calling the magic service.
    $agi_service = new AgiSchemaService();

    if ($current_node && isset($variables['content']['#average_review'])) {
      $schema = [
        '@context' => 'https://schema.org',
        '@type' => 'Product',
        'name' => $current_node->label(),
        'description' => $current_node->label() . ' from American Graphics Institute.',
        'sku' => $current_node->label(),
        'mpn' => strtolower(str_replace(' ', '-', $current_node->label())) . '-' . $current_node->id(),
        'productID' => 'Thing',
        'image' => $agi_service->getProductImage($current_node),
        'brand' => [
          '@type' => 'Brand',
          'name' => 'AGI' . $current_node->label(),
        ],
        'offers' => [
          '@type' => 'AggregateOffer',
          'offerCount' => 10,
          'lowPrice' => 450,
          'highPrice' => 995,
          'priceCurrency' => 'USD',
        ],
        'aggregateRating' => [
          '@type' => 'AggregateRating',
          'ratingValue' => $variables['content']['#average_review'],
          'reviewCount' => $variables['content']['#total_reviewers'],
          'worstRating' => 1,
          'bestRating' => 5,
        ],
        'review' => [
          '@type' => 'Review',
          'description' => $variables['content']['#latest_review'],
          'datePublished' => $variables['content']['#latest_review_date'],
          'author' => [
            '@type' => 'Person',
            'name' => $variables['content']['#latest_reviewer'],
          ],
          'itemReviewed' => [
            '@type' => 'Thing',
            'name' => $current_node->label(),
          ],
          'reviewRating' => [
            '@type' => 'Rating',
            'worstRating' => 1,
            'bestRating' => 5,
            'ratingValue' => $variables['content']['#average_review'],
          ],
        ],
      ];

      $variables['#attached']['html_head'][] = [
        [
          '#tag' => 'script',
          '#attributes' => [
            'type' => 'application/ld+json',
          ],
          '#value' => json_encode($schema),
        ],
        'agi_review_schema',
      ];
    }
  }
}

/**
 * Implements hook_preprocess_node().
 */
function agi_schema_preprocess_node(&$variables) {
  $base_path = \Drupal::request()->getSchemeAndHttpHost();
  $node_type = $variables['node']->getType();

  // Calling the magic service.
  $agi_service = new AgiSchemaService();
  $site_logo = \Drupal::theme()->getActiveTheme()->getLogo();

  if ($node_type === 'course_date' && $variables['node']->field_cd_event->target_id) {
    $site_logo = \Drupal::theme()->getActiveTheme()->getLogo();
    $event = ProductVariation::load($variables['node']->field_cd_event->target_id);

    if ($event) {
      $event_date = $event->hasField('field_event_start_date') ? $event->field_event_start_date->first()->getValue() : NULL;
      $event_start_date = date('Y-m-d', strtotime($event_date['value']));
      $event_end_date = date('Y-m-d', strtotime($event_date['end_value']));
      $event_location = $event->field_event_location->entity->field_city_for_reference_pages->value;
      $event_url = $base_path . \Drupal::service('path_alias.manager')->getAliasByPath('/node/' . $variables['node']->field_cd_event->target_id);

      // Treating online events.
      if ($event_location === 'Online') {
        $event_location_data = [
          '@type' => 'VirtualLocation',
          'url' => $event_url,
        ];
      } else {
        $event_location_data = [
          '@type' => 'Place',
          'name' => $event_location,
          'address' => [
            '@type' => 'PostalAddress',
            'name' => $event_location,
          ],
        ];
      }

      $schema = [
        '@context' => 'https://schema.org',
        '@type' => 'EducationEvent',
        'name' => $event->label(),
        'url' => $event_url,
        'image' => $base_path . '/' . $site_logo,
        'eventStatus' => 'https://jsonld.com/event/concert-multiple-performers/',
        'eventAttendanceMode' => $event_location === 'Online' ? 'http://schema.org/OnlineEventAttendanceMode' : 'http://schema.org/OfflineEventAttendanceMode',
        'startDate' => $event_start_date,
        'endDate' => $event_end_date,
        'description' => $event->label() . ', ' . $event_location . ' on ' . $event_start_date,
        'eventStatus' => 'http://schema.org/EventScheduled',
        'performer' => [
          '@type' => 'Organization',
          'name' => 'American Graphics Institute',
        ],
        'organizer' => [
          '@type' => 'Organization',
          'name' => 'American Graphics Institute',
          'url' => 'https://www.agitraining.com/',
        ],
        'location' => $event_location_data,
        'offers' => [
          '@type' => 'Offer',
          'url' => $event_url,
          'validFrom' => '1969-12-31T19:00:00-05:00',
          'priceCurrency' => 'USD',
          'availability' => 'http://schema.org/InStock',
          'price' => number_format($event->commerce_price->first()->getValue()['number'], 2, '.', ''),
        ],
      ];
    }

    $variables['#attached']['html_head'][] = [
      [
        '#tag' => 'script',
        '#attributes' => [
          'type' => 'application/ld+json',
        ],
        '#value' => json_encode($schema),
      ],
      'agi_course_schema',
    ];
  }
  else if ($node_type === 'seconday_location_category_page') {
    $location = $variables['node']->field_secondary_location->entity;
    $category = $variables['node']->field_cat_for_location->entity;
    $fids = [];

    $images_media = $location->get('field_location_image_media')->getValue();

    if (!empty($images_media)) {
      foreach ($images_media as $image_media) {
        $media = Media::load($image_media['target_id']);

        if ($media->field_media_image->entity) {
          $fids[] = file_create_url($media->field_media_image->entity->getFileUri());
        }
      }
    }

    $schema = [
      '@context' => 'https://schema.org',
      '@type' => 'EducationalOrganization',
      'name' => 'AGI Training ' . $location->label(),
      'image' => $fids,
      'description' => 'Offering ' . $category->field_short_category_name->value . ' classes and ' . $category->field_short_category_name->value . ' training in ' . $location->label(),
      'address' => [
        '@type' => 'PostalAddress',
        'streetAddress' => $location->field_address_street->value,
        'addressLocality' => $location->field_address_city->value,
        'addressRegion' => $location->field_state_abbreviated_->value,
        'postalCode' => $location->field_address_zip->value,
        'telephone' => '(800) 851-9237',
        'addressCountry' => [
          '@type' => 'Country',
          'name' => 'USA',
        ],
      ],
    ];

    $variables['#attached']['html_head'][] = [
      [
        '#tag' => 'script',
        '#attributes' => [
          'type' => 'application/ld+json',
        ],
        '#value' => json_encode($schema),
      ],
      'agi_location_schema',
    ];
  }
  else if ($node_type === 'seconday_location_page') {
    $location = $variables['node'];
    $images_media = $location->get('field_location_image_media')->getValue();

    if (!empty($images_media)) {
      foreach ($images_media as $image_media) {
        $media = Media::load($image_media['target_id']);

        if ($media->field_media_image->entity) {
          $fids[] = file_create_url($media->field_media_image->entity->getFileUri());
        }
      }
    }

    $schema = [
      '@context' => 'https://schema.org',
      '@type' => 'EducationalOrganization',
      'name' => 'AGI Training ' . $location->label(),
      'image' => $fids,
      'address' => [
        '@type' => 'PostalAddress',
        'streetAddress' => $location->field_address_street->value,
        'addressLocality' => $location->field_address_city->value,
        'addressRegion' => $location->field_state_abbreviated_->value,
        'postalCode' => $location->field_address_zip->value,
        'telephone' => '(800) 851-9237',
        'addressCountry' => [
          '@type' => 'Country',
          'name' => 'USA',
        ],
      ],
    ];

    $variables['#attached']['html_head'][] = [
      [
        '#tag' => 'script',
        '#attributes' => [
          'type' => 'application/ld+json',
        ],
        '#value' => json_encode($schema),
      ],
      'agi_secondary_location_schema',
    ];
  }
  else if ($node_type === 'location' || $node_type === 'category_location_page') {
    $node = $node_type === 'location' ? $variables['node'] : $variables['node']->field_location_for_cat->entity;

    $schema = [
      '@context' => 'https://schema.org',
      '@type' => 'School',
      'name' => 'American Graphics Institute',
      'description' => $node->field_microdata_description->value,
      'image' => $agi_service->getProductImage($node),
      'url' => $base_path,
      'alternateName' => $node->field_location_address_title->value,
      'address' => [
        '@type' => 'PostalAddress',
        'streetAddress' => $node->field_location_address->value,
        'addressLocality' => $node->field_location_city->value,
        'addressRegion' => $node->field_location_state->value,
        'postalCode' => $node->field_location_zip->value,
        'telephone' => $node->field_location_tel->value,
        'addressCountry' => [
          '@type' => 'Country',
          'name' => 'USA',
        ],
      ],
    ];

    $variables['#attached']['html_head'][] = [
      [
        '#tag' => 'script',
        '#attributes' => [
          'type' => 'application/ld+json',
        ],
        '#value' => json_encode($schema),
      ],
      'agi_location_school_schema',
    ];
  }
}
