<?php

namespace Drupal\agi_course_date;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\views\Views;

class OldCourseDateService {
  /**
   * Loading the entity type manager.
   */
  protected $entityTypeManager;

  /**
   * Contruct method to initiate the EntityTypeManager.
   */
  public function __construct(EntityTypeManager $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Unpublishing the past course date nodes.
   */
  public function unpublish() {
    // Get and loop through the View
    $view = Views::getView('course_date_vbo');
    $view->setDisplay('course_date_nids');
    $view->execute();

    // Get the results of the view.
    $view_result = $view->result;

    // Check if the view is not empty and return results.
    if (!empty($view_result)) {
      foreach ($view->result as $row) {
        $node = $row->_entity;
        $node->setPublished(FALSE);
        $node->save();
      }
    }
  }
}
