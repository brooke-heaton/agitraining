<?php

namespace Drupal\agi_quiz\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for AGI Quiz routes.
 */
class AgiQuizController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t(''),
    ];

    return $build;
  }

}
