<?php

namespace Drupal\agi_quiz\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides an example block.
 *
 * @Block(
 *   id = "agi_quiz_example",
 *   admin_label = @Translation("Example"),
 *   category = @Translation("AGI Quiz")
 * )
 */
class ExampleBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build['content'] = [
      '#markup' => $this->t(''),
    ];
    return $build;
  }

}
