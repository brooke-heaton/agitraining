<?php

namespace Drupal\agi_quiz_migration\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Provides a 'SummaryAsBody' migrate process plugin.
 *
 * @MigrateProcessPlugin(
 *  id = "default_quiz_results"
 * )
 */
class DefaultQuizResults extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $output = [
      'end' => [
        'quiz_question_view_question' => 'quiz_question_view_question',
        'choice' => 'choice',
        'correct' => 'correct',
        'score' => 'score',
        'quiz_question_view_full' => 'quiz_question_view_full',
        'attempt' => 0,
        'answer_feedback' => 'answer_feedback',
        'question_feedback' => 0,
        'solution' => 'solution',
        'quiz_feedback' => 0,
      ],
      'question' => [
        'quiz_question_view_full' => 0,
        'quiz_question_view_question' => 0,
        'attempt' => 0,
        'choice' => 0,
        'correct' => 0,
        'score' => 0,
        'answer_feedback' => 0,
        'question_feedback' => 0,
        'solution' => 0,
        'quiz_feedback' => 0,
      ],
    ];
    return serialize($output);
  }

}
