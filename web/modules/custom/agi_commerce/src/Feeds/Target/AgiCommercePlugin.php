<?php

namespace Drupal\agi_commerce\Feeds\Target;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\feeds\FieldTargetDefinition;
use Drupal\feeds\Plugin\Type\Target\ConfigurableTargetInterface;
use Drupal\feeds\Plugin\Type\Target\FieldTargetBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for Commerce plugin item fields.
 */
abstract class AgiCommercePlugin extends FieldTargetBase implements ConfigurableTargetInterface, ContainerFactoryPluginInterface {

  /**
   * The plugin manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $pluginManager;

  /**
   * Constructs a new CommercePlugin object.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin id.
   * @param array $plugin_definition
   *   The plugin definition.
   * @param \Drupal\commerce_promotion\PromotionOfferManager $promotion_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, PluginManagerInterface $plugin_manager) {
    $this->pluginManager = $plugin_manager;
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  protected static function prepareTarget(FieldDefinitionInterface $field_definition) {
    return FieldTargetDefinition::createFromFieldDefinition($field_definition)
      ->addProperty('target_plugin_id')
      ->addProperty('target_plugin_configuration');
  }

  /**
   * {@inheritdoc}
   */
  protected function prepareValue($delta, array &$values) {
    if (empty($values['target_plugin_id'])) {
      $values['target_plugin_id'] = $this->configuration['target_plugin_id'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['target_plugin_id' => NULL];
  }

  /**
   * Returns the available promotion offer types.
   */
  protected function getTargetPluginIdOptions() {
    $options = [];
    foreach ($this->pluginManager->getDefinitions() as $definition) {
      $options[$definition['id']] = $definition['label'];
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['target_plugin_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Type'),
      '#options' => $this->getTargetPluginIdOptions(),
      '#default_value' => $this->configuration['target_plugin_id'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    return [
      'target_plugin_id' => [
        '#type' => 'item',
        '#markup' => $this->t('Type: %type', [
          '%type' => $this->getTargetPluginIdOptions()[$this->configuration['target_plugin_id']] ?? $this->t('None'),
        ]),
      ],
    ];
  }

}
