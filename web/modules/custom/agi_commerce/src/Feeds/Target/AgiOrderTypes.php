<?php

namespace Drupal\agi_commerce\Feeds\Target;

use Drupal\feeds\Feeds\Target\EntityReference;

/**
 * Defines a commerce_price field mapper.
 *
 * @FeedsTarget(
 *   id = "order_types",
 *   field_types = {"order_types"}
 * )
 */
class AgiOrderTypes extends EntityReference {

}
