<?php

namespace Drupal\agi_commerce;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Queue\QueueFactory;

/**
 * Default cron implementation.
 *
 * Queues outdated classes for cart removal.
 *
 * @see \Drupal\agi_commerce\Plugin\QueueWorker\CartCleanup
 */
class Cron implements CronInterface {

  /**
   * The order storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $orderStorage;


  /**
   * The agi_commerce_cart_cleanup queue.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $queue;

  /**
   * Constructs a new Cron object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue factory.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, QueueFactory $queue_factory) {
    $this->orderStorage = $entity_type_manager->getStorage('commerce_order');
    $this->queue = $queue_factory->get('agi_commerce_cart_cleanup');
  }

  /**
   * {@inheritdoc}
   */
  public function run() {
    $all_order_ids = $this->getOrderIds();
    foreach (array_chunk($all_order_ids, 100) as $order_ids) {
      $this->queue->createItem($order_ids);
    }
  }

  /**
   * Gets the applicable order IDs.
   *
   * @return array
   *   The order IDs.
   */
  protected function getOrderIds() {
    $ids = $this->orderStorage->getQuery()
      ->condition('type', 'default')
      ->condition('cart', TRUE)
      ->accessCheck(FALSE)
      ->addTag('agi_commerce')
      ->execute();
    return $ids;
  }

}
