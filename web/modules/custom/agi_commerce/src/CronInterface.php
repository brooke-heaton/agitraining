<?php

namespace Drupal\agi_commerce;

/**
 * Provides the interface for cron.
 */
interface CronInterface {

  /**
   * Runs the cron.
   */
  public function run();

}
