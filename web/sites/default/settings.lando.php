<?php

if ($lando_info = getenv('LANDO_INFO')) {
  $lando_info = json_decode($lando_info, TRUE);

  $databases['default']['default'] = [
    'driver' => 'mysql',
    'database' => $lando_info['database']['creds']['database'],
    'username' => $lando_info['database']['creds']['user'],
    'password' => $lando_info['database']['creds']['password'],
    'host' => $lando_info['database']['internal_connection']['host'],
    'port' => $lando_info['database']['internal_connection']['port'],
  ];

  $config['config_split.config_split.local']['status'] = TRUE;


}
